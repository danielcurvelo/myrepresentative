//
//  Senator.h
//  MyRepresentative
//
//  Created by Mac User on 3/1/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@import UIKit;

static NSString *nameKey = @"name";
static NSString *stateKey = @"state";
static NSString *districtKey = @"district";
static NSString *phoneKey = @"phone";
static NSString *officeKey = @"office";
static NSString *websiteKey = @"link";
static NSString *partyKey = @"party";
static NSString *resultsKey = @"results";

@interface Senator : NSManagedObject

@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * state;
@property (nonatomic, copy) NSString * district;
@property (nonatomic, copy) NSString * phone;
@property (nonatomic, copy) NSString * website;
@property (nonatomic, copy) NSString * party;
@property (nonatomic, copy) NSString * office;
@property (nonatomic, copy) NSOrderedSet *entries;

@end

@interface Senator (CoreDataGeneratedAccessors)

- (void)insertObject:(NSManagedObject *)value inEntriesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromEntriesAtIndex:(NSUInteger)idx;
- (void)insertEntries:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeEntriesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInEntriesAtIndex:(NSUInteger)idx withObject:(NSManagedObject *)value;
- (void)replaceEntriesAtIndexes:(NSIndexSet *)indexes withEntries:(NSArray *)values;
- (void)addEntriesObject:(NSManagedObject *)value;
- (void)removeEntriesObject:(NSManagedObject *)value;
- (void)addEntries:(NSOrderedSet *)values;
- (void)removeEntries:(NSOrderedSet *)values;
@end
