//
//  ViewController.m
//  MyRepresentative
//
//  Created by Mac User on 2/28/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "ViewController.h"
#import "UIColor+CustomColors.h"
#import "NSAttributedString+CustomFont.h"
#import "StateSelectionViewController.h"
#import "SecondSenatorViewController.h"
#import "ModelController.h"
#import "CollectionViewDataSource.h"
#import "Senator.h"

@interface ViewController ()

@property (nonatomic,strong) UILabel *stateLabel;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) CollectionViewDataSource *dataSource;
@property (nonatomic,strong) Senator *firstSenator;
@end

@implementation ViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.dataSource = [CollectionViewDataSource new]; //Initialize DataSource
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor patrioticRed]];
    screenWidth = self.view.frame.size.width;
    
// STATE SELECTION CHECK - Here we are checking if the user has selected a state previously. If not we'll present a modal with state options.
    
    if ([ModelController currentState] == nil) {
        StateSelectionViewController *selectState = [StateSelectionViewController new];
        [self presentViewController:selectState animated:YES completion:nil];
    }
    
    [self setupNavBar];
    [self setupCollectionView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

        [self setupViews]; // Setup Views only if we have data available
        [self setViewControllerTitle];
}

#pragma mark - Setup Methods (Titles, NavBar, moreViews)

- (void)setViewControllerTitle
{
    if ([ModelController currentState]) {
        Senator *firstSenator = [[ModelController sharedInstance].senators firstObject];
        [self setTitle:firstSenator.name];
        [self.tabBarItem setImage:[UIImage imageNamed:@"senator"]];
        Senator *secondSenator = [[ModelController sharedInstance].senators lastObject];
        [[self.tabBarController.viewControllers objectAtIndex:1] setTitle:secondSenator.name];
    }
}

- (void)setupNavBar
{
    UIBarButtonItem *changeStateBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"us"]
                                                                      style:UIBarButtonItemStyleDone
                                                                     target:self
                                                                     action:@selector(changeState)];
    [self.navigationItem setLeftBarButtonItem:changeStateBtn];

    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                           target:self
                                                                           action:@selector(addNote)]; //Feature for next Build
    [self.navigationItem setRightBarButtonItem:addBtn];
}

-(void)setupViews // For Labels and buttons related to Senator's info.
{
    self.firstSenator = [[ModelController sharedInstance].senators firstObject]; // Pointer to the first senator in the array
    
    if (!self.stateLabel) {
        self.stateLabel= [[UILabel alloc]initWithFrame:CGRectMake((screenWidth/2) - 25, 70, 50, 50)];
        [self.stateLabel setTextAlignment:NSTextAlignmentCenter];
        [self.stateLabel setAttributedText:[NSAttributedString setAttributedTitle:[ModelController currentState]]];
        [self.stateLabel.layer setBorderColor:[UIColor patrioticBlue].CGColor];
        [self.stateLabel.layer setBorderWidth:1.0];
        [self.stateLabel.layer setCornerRadius:25];
        [self.view addSubview:self.stateLabel];
        
        self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 140, 160, 40)];
        [self.nameLabel setTextColor:[UIColor whiteColor]];
        [self.nameLabel setText:self.firstSenator.name];
        [self.view addSubview:self.nameLabel];
    }
    else
    {
        [self.stateLabel setAttributedText:[NSAttributedString setAttributedTitle:[ModelController currentState]]];
        [self.nameLabel setText:self.firstSenator.name];

    }
    
    UIButton *websiteBtn = [[UIButton alloc]initWithFrame:CGRectMake((screenWidth - 110), 140, 90, 40)];
    [websiteBtn setBackgroundColor:[UIColor patrioticBlue]];
    [websiteBtn setTitle:@"Website" forState:UIControlStateNormal];
    [websiteBtn addTarget:self action:@selector(goToWebsite) forControlEvents:UIControlEventTouchUpInside];
    [websiteBtn.layer setCornerRadius:8.0];
    [websiteBtn.layer setBorderColor:[UIColor patrioticBlueShadow].CGColor];
    [websiteBtn.layer setBorderWidth:1.0];
    [self.view addSubview:websiteBtn];
    
}

#pragma mark - Button Target Actions (Change State, Add Note, Website)

- (void)changeState
{
    StateSelectionViewController *selectState = [StateSelectionViewController new];
    [self presentViewController:selectState animated:YES completion:nil];
}

- (void)addNote
{
    //Add Entry Method
}

- (void)goToWebsite
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.firstSenator.website]]; // It will open safari for now (End result: Native Browser/WebView Controller)
}

#pragma mark - UICollection View

- (void)setupCollectionView
{
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, self.view.center.y - 20, screenWidth, self.view.frame.size.height / 2) collectionViewLayout:layout];
    [self.collectionView setDataSource:self.dataSource];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.dataSource registerCollectionView:self.collectionView];
    [self.collectionView setDelegate:self];
    [self.view addSubview:self.collectionView];
}

#pragma mark UICollectionView Delegat Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize itemSize = CGSizeMake((screenWidth /2) - 10 , 80); // Set the size of cell
    return itemSize;
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, 5, 0, 5); // 5 px inset for left and right
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
