//
//  ModelController.m
//  MyRepresentative
//
//  Created by Mac User on 2/28/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "ModelController.h"
#import "NetworkController.h"
#import "Senator.h"
#import "Stack.h"

static NSString * currentState;

@implementation ModelController

+ (ModelController *)sharedInstance {
    static ModelController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [ModelController new];
        });
    return sharedInstance;
}

#pragma mark - Senators Array & Load Senators methods

- (NSArray *)senators
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Senator"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"state = %@", [ModelController currentState]];
    [fetchRequest setFetchLimit:2];
    [fetchRequest setPredicate:predicate];
    NSArray *objects = [[Stack sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:NULL];
    return objects;
}

- (void)loadSenators
{
    if ([ModelController currentState]) {
    
    NSString *path = [NSString stringWithFormat:@"getall_sens_bystate.php?state=%@&output=json", [ModelController currentState]];
    
    [[NetworkController api] GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSArray *resultSenators = [[NSArray alloc]initWithArray:[responseObject objectForKey:resultsKey]];
        NSLog(@"%@",responseObject); // response object description
        
        for (NSDictionary *dictionary in resultSenators) {
            Senator *senator = [NSEntityDescription
                                insertNewObjectForEntityForName:@"Senator"
                                inManagedObjectContext:[Stack sharedInstance].managedObjectContext];

            senator.name = dictionary[nameKey];
            senator.state = dictionary[stateKey];
            senator.phone = dictionary[phoneKey];
            senator.party = dictionary[partyKey];
            senator.district = dictionary[districtKey];
            senator.office = dictionary[officeKey];
            senator.website = dictionary[websiteKey];
            
        }
        
        [ModelController sharedInstance].senators = [[NSArray alloc] initWithArray:resultSenators];
        [self save];

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"There ahs been an error: %@",error);
    }];
    }
}

#pragma mark - Remove Senators

- (void)removeSenators
{
    for (NSManagedObject * senators in [ModelController sharedInstance].senators) {
        [[Stack sharedInstance].managedObjectContext deleteObject:senators];
    }
    [self save];
}

#pragma mark - Save to Persistant Store

- (void)save {
    
    [[Stack sharedInstance].managedObjectContext save:NULL];
    
}

#pragma mark - Set State Methods // NSUserDefaults

+ (void)setCurrentState:(NSString *)state
{
    [[NSUserDefaults standardUserDefaults] setObject:state forKey:stateKey];
}

+ (NSString *)currentState
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:stateKey];
}

@end
