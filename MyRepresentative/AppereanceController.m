//
//  AppereanceController.m
//  
//
//  Created by Mac User on 3/1/15.
//
//

#import "AppereanceController.h"
#import "UIColor+CustomColors.h"
@import UIKit;

@implementation AppereanceController

+ (void)initializeAppereanceDefaults
{
// NAVIGATION BAR APPEARANCE
    [[UINavigationBar appearance]setBarTintColor:[UIColor patrioticRed]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:18]}]; //White tint
// TAB BAR APPEARANCE
    [[UITabBar appearance] setBarTintColor:[UIColor patrioticBlue]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
// STATUS BAR APPEARANCE
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


@end
