//
//  Senator.m
//  MyRepresentative
//
//  Created by Mac User on 3/1/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "Senator.h"

@implementation Senator

@dynamic name;
@dynamic state;
@dynamic district;
@dynamic phone;
@dynamic website;
@dynamic office;
@dynamic party;
@dynamic entries;


@end
