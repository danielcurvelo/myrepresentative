//
//  NetworkController.h
//  MyRepresentative
//
//  Created by Mac User on 2/28/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface NetworkController : NSObject

+ (AFHTTPSessionManager *)api; //Session Manager singleton with Base URL

@end
