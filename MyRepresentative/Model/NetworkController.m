//
//  NetworkController.m
//  MyRepresentative
//
//  Created by Mac User on 2/28/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "NetworkController.h"

static NSString * apiBaseURL = @"http://whoismyrepresentative.com";

@implementation NetworkController

+ (AFHTTPSessionManager *)api {
    
    static AFHTTPSessionManager *api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:apiBaseURL]];
        api.responseSerializer = [AFJSONResponseSerializer serializer];
        api.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"]; // Specify Content Type
    });
    return api;
}

@end
