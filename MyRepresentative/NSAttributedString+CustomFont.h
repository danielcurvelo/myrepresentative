//
//  NSAttributedString+CustomFont.h
//  MyRepresentative
//
//  Created by Mac User on 3/1/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (CustomFont)

+ (NSAttributedString *)stringWithWhite:(NSString *)string;

+ (NSAttributedString *)setAttributedTitle:(NSString *)string;

@end
