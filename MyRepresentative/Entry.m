//
//  Entry.m
//  MyRepresentative
//
//  Created by Mac User on 3/1/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "Entry.h"
#import "Senator.h"


@implementation Entry

@dynamic title;
@dynamic text;
@dynamic senator;

@end
