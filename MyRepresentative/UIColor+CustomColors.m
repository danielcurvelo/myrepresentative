//
//  UIColor+CustomColors.m
//  MyRepresentative
//
//  Created by Mac User on 3/1/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+ (UIColor *)patrioticBlue
{
    return [UIColor colorWithRed:0.1 green:0.29 blue:0.49 alpha:1];
}

+ (UIColor *)patrioticBlueShadow;
{
    return [UIColor colorWithRed:0.16 green:0.21 blue:0.32 alpha:1];
}

+ (UIColor *)patrioticRed
{
    return [UIColor colorWithRed:0.63 green:0.14 blue:0.15 alpha:1];
}

+ (UIColor *)patrioticRedShadow
{
    return [UIColor colorWithRed:0.56 green:0.12 blue:0.12 alpha:1];
}

@end
