//
//  CustomCollectionViewCell.h
//  MyRepresentative
//
//  Created by Mac User on 3/2/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) UILabel *titleLabel;
-(void)updateWithTitle:(NSString *)title;

@end
