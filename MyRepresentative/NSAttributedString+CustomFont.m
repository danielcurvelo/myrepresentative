//
//  NSAttributedString+CustomFont.m
//  MyRepresentative
//
//  Created by Mac User on 3/1/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "NSAttributedString+CustomFont.h"
@import UIKit;

@implementation NSAttributedString (CustomFont)

+ (NSAttributedString *)stringWithWhite:(NSString *)string
{
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attributedString;
}

+ (NSAttributedString *)setAttributedTitle:(NSString *)string // To be used when there's a header Title
{
    return [[NSAttributedString alloc] initWithString:string attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:24]}];
}

@end
