//
//  PickerData.h
//  MyRepresentative
//
//  Created by Mac User on 2/28/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PickerData : NSObject

+(NSArray *)states;

@end
