//
//  CollectionViewDataSource.m
//  
//
//  Created by Mac User on 3/2/15.
//
//

#import "CollectionViewDataSource.h"
#import "CustomCollectionViewCell.h"

@implementation CollectionViewDataSource

- (void)registerCollectionView:(UICollectionView *)collectionView;
{
    [collectionView registerClass:[CustomCollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4; // Hard coded until <Model Controller> can handle entries.
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CustomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell updateWithTitle:[NSString stringWithFormat:@"Note %ld",(long)indexPath.row + 1]]; // Hard coded until <Model Controller> can handle entries.
    return cell;
}

@end
