//
//  StateSelectionViewController.m
//  MyRepresentative
//
//  Created by Mac User on 2/28/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "StateSelectionViewController.h"
#import "UIColor+CustomColors.h"
#import "NSAttributedString+CustomFont.h"
#import "PickerDataSource.h"
#import "PickerData.h"
#import "ModelController.h"
#import "ViewController.h"
#import "Senator.h"
#import "SecondSenatorViewController.h"

@interface StateSelectionViewController () <UIPickerViewDelegate>

@property (strong,nonatomic) PickerDataSource *dataSource;
@property (strong,nonatomic) UIPickerView *statePicker;

@end

@implementation StateSelectionViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.dataSource = [PickerDataSource new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
// Setting up screenValues
    screenWidth = self.view.frame.size.width;

// Loading Picker and Buttons
    [self setPicker];
    [self setSaveButton];
    [self setPageTitle];

}

- (void)setPageTitle
{
    UILabel *pageTitle = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth/2) - 85, self.statePicker.frame.origin.y - 15 , 170, 30)];
    [pageTitle setAttributedText:[NSAttributedString setAttributedTitle:@"Choose a State"]];
    [pageTitle setTextAlignment:NSTextAlignmentCenter];
    [pageTitle setShadowColor:[UIColor patrioticBlueShadow]];
    [pageTitle setShadowOffset:CGSizeMake(1.0, 3.0)];
    [self.view addSubview:pageTitle];
}

#pragma mark - Setting PickerView

- (void)setPicker
{
    [self.view setBackgroundColor:[UIColor patrioticBlue]];
    self.statePicker = [[UIPickerView alloc]init];
    [self.statePicker setCenter:[self.view center]];
    [self.statePicker setDelegate:self];
    [self.statePicker setDataSource:self.dataSource];
    [self.view addSubview:self.statePicker];
}

// UIPICKERVIEW DELEGATE METHOD

-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *data = [NSString stringWithFormat:@"%@",[[PickerData states] objectAtIndex:row]];
    NSAttributedString *title = [NSAttributedString stringWithWhite:data];
    
    return title;
}

#pragma mark - Setting Up Save Button

- (void)setSaveButton
{
    CGFloat btnY = (self.statePicker.frame.origin.y + self.statePicker.frame.size.height) + 15;
    UIButton *saveBtn = [[UIButton alloc]initWithFrame:CGRectMake((screenWidth/2) - 40, btnY , 80, 30)];
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    [saveBtn.layer setCornerRadius:8.0];
    [saveBtn.layer setBorderColor:[UIColor patrioticBlueShadow].CGColor];
    [saveBtn.layer setBorderWidth:1.0];
    [saveBtn setBackgroundColor:[UIColor patrioticRed]];
    [saveBtn addTarget:self action:@selector(saveState) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
}

// SAVE BTN TARGET/ACTION

- (void)saveState
{
    [[ModelController sharedInstance] removeSenators];
    NSString *selectedState = [NSString stringWithFormat:@"%@",[[PickerData states] objectAtIndex:[self.statePicker selectedRowInComponent:0]]];
    [ModelController setCurrentState:selectedState];
    [[ModelController sharedInstance] loadSenators];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
