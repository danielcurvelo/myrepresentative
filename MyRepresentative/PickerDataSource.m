//
//  PickerDataSource.m
//  MyRepresentative
//
//  Created by Mac User on 2/28/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "PickerDataSource.h"
#import "PickerData.h"

@implementation PickerDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[PickerData states] count];
}

@end
