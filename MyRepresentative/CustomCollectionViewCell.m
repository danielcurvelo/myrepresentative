//
//  CustomCollectionViewCell.m
//  MyRepresentative
//
//  Created by Mac User on 3/2/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "CustomCollectionViewCell.h"
#import "UIColor+CustomColors.h"

@implementation CustomCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupViewWithFrame:frame]; // Add Label and style to cell
    }
    return self;
}

-(void)setupViewWithFrame:(CGRect)frame
{
    self.titleLabel = [[UILabel alloc]initWithFrame:self.bounds];
    [self setBackgroundColor:[UIColor patrioticRedShadow]];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.layer setBorderWidth:1.0];
    [self.layer setCornerRadius:8.0];
    [self addSubview:self.titleLabel];
}

-(void)updateWithTitle:(NSString *)title
{
    [self.titleLabel setText:title];
}

@end
