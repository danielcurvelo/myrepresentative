//
//  ModelController.h
//  MyRepresentative
//
//  Created by Mac User on 2/28/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ModelController;
@class Senator;

static NSString * const senatorsKey = @"Senator";

@interface ModelController : NSObject

@property (nonatomic, copy) NSArray *senators;

+ (ModelController *)sharedInstance;

- (void)loadSenators;
- (void)removeSenators;

+ (void)setCurrentState:(NSString *)state;
+ (NSString *)currentState;


@end
