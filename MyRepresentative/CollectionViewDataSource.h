//
//  CollectionViewDataSource.h
//  
//
//  Created by Mac User on 3/2/15.
//
//

#import <Foundation/Foundation.h>
@import UIKit;

static NSString * const cellIdentifier = @"cell";

@interface CollectionViewDataSource : NSObject <UICollectionViewDataSource>

- (void)registerCollectionView:(UICollectionView *)collectionView;

@end
