//
//  UIColor+CustomColors.h
//  MyRepresentative
//
//  Created by Mac User on 3/1/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+ (UIColor *)patrioticBlue;

+ (UIColor *)patrioticBlueShadow;

+ (UIColor *)patrioticRed;

+ (UIColor *)patrioticRedShadow;

@end
